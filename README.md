# The example project of C# for training
This project was made for intending as a project for new to C# developing.

# Appearance
![](images/capture01.png)

|No.|Description|
|--|--|
|1| Push button, opens the messagebox when user pushed this|
|2| Textbox, user can enter the text|
|3| Label, displays the text that entered in the TextBox in realtime|

# Environment
|||
|--|--|
|Programming language| C# |
|Framework |  .NET Framework 4.7 (legacy framework)|
|GUI subsystem |  wpf (xaml)|
|Host PC| Windows10 Professional (64 bit)|
|IDE | Visual Studio 2022 community edition|

# Usage
Clone this repository to your local drive using the following command.
```bash
git clone https://gitlab.com/j-project-public/csexample.git
```
Then, make your working branch using the following command.

```bash
git checkout -b <your branch name>
```
For example, if you want to make the mystudy branch, please type the following command on the git bash.
```bash
git checkout -b mystudy
```
this command makes the mystudy branch and change the working tree to it.

