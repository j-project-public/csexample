﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CsExample
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            // Call the OnStartup function in base class
            base.OnStartup(e);

            // Creates the instance of the MainWindow
            MainWindow m_mainwnd = new MainWindow();

            // Displays the MainWindow on the desktop
            m_mainwnd.Show();
        }
    }
}
