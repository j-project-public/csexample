﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CsExample
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window,INotifyPropertyChanged
    {

        private string m_editbox_text;

        public event PropertyChangedEventHandler PropertyChanged;

        public string p_editbox_text
        {
            set
            {
                m_editbox_text = value;
                updateProperty(nameof(p_editbox_text));
            }
            get
            {
                return m_editbox_text;
            }
        }

        /// <summary>
        /// Notify the property updated to the UI (MainWindow.xaml)
        /// </summary>
        /// <param name="v">The string that contains the name of the property</param>
        private void updateProperty(string v)
        {
            PropertyChanged?.Invoke(this,new PropertyChangedEventArgs(v));
        }

        public MainWindow()
        {
            InitializeComponent();

            // In this example, the view and ViewModel is same class so set this instance to the DataContext of the MainWindow
            this.DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Show the message box
            MessageBox.Show(
                //"The button has pushed","Message",MessageBoxButton.OK,MessageBoxImage.Information
                $"The text in the box is {p_editbox_text}", "Message", MessageBoxButton.OK, MessageBoxImage.Information
                );
        }
    }
}
